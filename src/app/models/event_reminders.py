from config import Config
from db.event_reminder import EventReminder
from db.event import Event
from pblib.mail_send import send_single_event_reminder_mail
import json

class Reminder:
    def __init__(self, reminder_id, queue_client):
        self.data = Config.db_session.query(EventReminder).filter(EventReminder.reminder_id == reminder_id).one_or_none()
        self.queue_client = queue_client

    def sendReminder(self):
        result = False
        if self.data is not None:
            reminder_definition = json.loads(self.data.definition)
            reminder_type = reminder_definition.get("reminder_type")
            if reminder_type == "gui":

                message = {"event": "showEasy2scheduleEventReminder",
                           "args": {"event_id": self.data.event_id}}
                for recipient in reminder_definition.get("recipients"):
                    payload = {"room": recipient, "data": message}
                    self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                              message=json.dumps(payload))
                result = True
            elif reminder_type == "email":
                print("SENT OUT THE FUCKING REMINDER MAIL HERE")
                event = Config.db_session.query(Event).filter(Event.event_id == self.data.event_id).one_or_none()
                event_payload = json.loads(event.payload)
                if event is not None:
                    for recipient in reminder_definition.get("recipients"):
                        send_single_event_reminder_mail(recipient=recipient, event_payload=event_payload)
                result = True

        return result
