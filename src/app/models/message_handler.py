import uuid

import requests
import json
from time import sleep
from copy import deepcopy
import os

from pbglobal.commands.easy2scheduleDeleteReminder import easy2scheduleDeleteReminder
from pblib.helpers import filter_dict
from pblib.amqp import client
from db.event import Event
from db.account import Account
from db.ressource import Ressource
from db.columns import Columns
from db.extended_properties import ExtendedProperties
from db.event_reminder import EventReminder
from config import Config

from pbglobal.events.newAppointmentCreated import newAppointmentCreated
from pbglobal.events.appointmentChanged import appointmentChanged
from pbglobal.events.appointmentDeleted import appointmentDeleted
from pbglobal.events.calendarRessourceCreated import calendarRessourceCreated
from pbglobal.events.calendarRessourceUpdated import calendarRessourceUpdated
from pbglobal.events.calendarRessourceDeleted import calendarRessourceDeleted
from pbglobal.events.calendarColumnsUpdated import calendarColumnsUpdated
from pbglobal.events.nextWeekReminderEmailsSent import nextWeekReminderEmailsSent
from pbglobal.events.easy2scheduleExtendedPropertiesSet import easy2scheduleExtendedPropertiesSet
from pbglobal.events.easy2scheduleReminderEmailConfigurationSet import easy2scheduleReminderEmailConfigurationSet
from pbglobal.events.easy2scheduleDataOwnerAddedToEvent import easy2scheduleDataOwnerAddedToEvent
from pbglobal.events.easy2scheduleDataOwnerRemovedFromEvent import easy2scheduleDataOwnerRemovedFromEvent
from pbglobal.events.appointmentReminderSent import appointmentReminderSent
from pbglobal.commands.setEasy2ScheduleEventReminder import setEasy2ScheduleEventReminder
from pbglobal.events.easy2ScheduleEventReminderSet import easy2ScheduleEventReminderSet
from pbglobal.events.easy2scheduleReminderDeleted import easy2scheduleReminderDeleted

from models.calendar_events import CalendarEvents
from models.event_reminders import Reminder

import traceback
from sqlalchemy.orm.exc import NoResultFound


def get_rooms_by_consumer_id(consumer_id):
    url = os.getenv("INTERNAL_ACCOUNTMANAGER_URL") + "/get_users"

    querystring = {"consumer_id": consumer_id}

    headers = {"X-Consumer-Custom-Id": consumer_id}

    response = requests.request("GET", url, headers=headers, params=querystring)
    if response.status_code == 500:
        return []

    data = response.json()
    rooms = []
    # print("GOT THESE ROOMS", response)
    for room in data:
        rooms.append(room.get("username"))

    return rooms


def filter_eventdata_for_user(login, data):
    result = json.loads(Account().getData(login=login))
    # print("ACOUNTFILTERDATA for",login, result)
    if result.get("event_data_filter") and data.get("extendedProps", {}).get("creator_login") != login:
        event_data_filter = result.get("event_data_filter")
    else:
        event_data_filter = {}

    # data = {**data, **event_data_filter}
    filtered_data = filter_dict(event_data_filter, deepcopy(data))
    # print("ACOUNTFILTERedDATA", filtered_data)
    return filtered_data


class message_handler():
    kong_api = "http://kong.planblick.svc:8001"
    queue_client = client()

    def handle_message(ch, method=None, properties=None, body=None):
        try:
            print("---------- Start handling Message ----------")
            event = method.routing_key
            body = body.decode("utf-8")
            instance = message_handler()
            if (event is not None):
                handler_exists = method.routing_key in dir(instance)

                if handler_exists is True:
                    if getattr(instance, method.routing_key)(ch, body) == True:
                        ch.basic_ack(method.delivery_tag)
                    else:
                        ch.basic_nack(method.delivery_tag)
                else:
                    raise Exception("No handler for this  message or even not responsible: '" + event + "'")

        except Exception as e:
            print(e)
            traceback.print_exc()
            ch.basic_nack(method.delivery_tag)
            sleep(15)

        print("---------- End handling Message ----------")

    def easy2scheduleReminderDeleted(self, channel, msg):
        try:
            print("Handling easy2scheduleReminderDeleted")
            command_payload = json.loads(msg)
            Config.db_session.query(EventReminder) \
                .filter(EventReminder.owner == command_payload.get("owner")) \
                .filter(EventReminder.reminder_id == command_payload.get("reminder_id")) \
                .delete()
            Config.db_session.commit()

            rooms = get_rooms_by_consumer_id(command_payload.get("owner"))
            message = {"event": "easy2scheduleReminderDeleted",
                       "args": {"correlation_id": command_payload.get("correlation_id"),
                                "reminder_id": command_payload.get("reminder_id")}}
            for room in rooms:
                payload = {"room": room, "data": message}
                channel.basic_publish(exchange="socketserver", routing_key="notifications", body=json.dumps(payload))

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False
        return True

    def easy2scheduleDeleteReminder(self, channel, msg):
        try:
            print("Handling easy2scheduleDeleteReminder")
            command_payload = json.loads(msg)
            event = easy2scheduleReminderDeleted.from_json(easy2scheduleReminderDeleted, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def nextWeekReminderEmailsSent(self, channel, msg):
        return True

    def updateCalendarRessource(self, channel, msg):
        try:
            print("Handling updateCalendarRessource")
            command_payload = json.loads(msg)
            event = calendarRessourceUpdated.from_json(calendarRessourceUpdated, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def calendarRessourceUpdated(self, channel, msg):
        try:
            payload_json = json.loads(msg)

            ressource = Config.db_session.query(Ressource) \
                .filter(Ressource.owner_id == payload_json.get("owner")) \
                .filter(Ressource.ressource_id == payload_json.get("ressource_id")) \
                .one_or_none()

            if ressource is None:
                message = {"event": "easy2scheduleRessourceUpdateFailed",
                           "args": {"correlation_id": payload_json.get("correlation_id"),
                                    "error_message": "Ressource not found"}}
                payload = {"room": payload_json.get("creator"), "data": message}
                self.queue_client.publish(toExchange="socketserver", routingKey="notifications",
                                          message=json.dumps(payload))
                return True

            ressource.display_name = payload_json.get("display_name", "not set")
            ressource.type = payload_json.get("type", "not set")
            ressource.props = json.dumps(payload_json.get("props", "{}"))
            ressource.save()

            ressource_event = {"id": ressource.id,
                               "display_name": payload_json.get("display_name"),
                               "type": payload_json.get("type"),
                               "props": payload_json.get("props"),
                               "ressource_id": ressource.ressource_id
                               }

            message = {"event": "calendarRessourceUpdated",
                       "args": ressource_event}
            payload = {"room": payload_json.get("owner"), "data": message}
            channel.basic_publish(exchange="socketserver", routing_key="notifications", body=json.dumps(payload))

        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False
        return True

    def easy2ScheduleEventReminderSet(self, channel, msg):
        try:
            msg = json.loads(msg)
            event_reminder = Config.db_session.query(EventReminder) \
                .filter(EventReminder.reminder_id == msg.get("reminder_id")) \
                .filter(EventReminder.owner == msg.get("owner")) \
                .filter(EventReminder.creator == msg.get("creator")) \
                .filter(EventReminder.event_id == msg.get("event_id")).one_or_none()

            if event_reminder is None:
                event_reminder = EventReminder()
                event_reminder.reminder_id = msg.get("reminder_id")
                event_reminder.owner = msg.get("owner")
                event_reminder.creator = msg.get("creator")
                event_reminder.event_id = msg.get("event_id")
                event_reminder.creation_time = msg.get("creation_time")

            event_reminder.data_owners = json.dumps([msg.get("creator")])
            event_reminder.seconds_in_advance = msg.get("seconds_in_advance")
            event_reminder.definition = json.dumps(msg)
            event_reminder.processed_time = None
            event_reminder.save()
        except Exception as e:
            print(e)
            traceback.print_exc()
            return False
        return True

    def setEasy2ScheduleEventReminder(self, channel, msg):
        try:
            print("Handling setEasy2ScheduleEventReminder")
            command_payload = json.loads(msg)
            event = easy2ScheduleEventReminderSet.from_json(easy2ScheduleEventReminderSet, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def appointmentReminderSent(self, channel, msg):
        try:
            message_payload = json.loads(msg)
            print("MESSAGE_PAYLOAD", message_payload)
            event = Config.db_session.query(EventReminder).filter(
                EventReminder.reminder_id == message_payload.get("reminder_id")).one_or_none()
            event_payload = json.loads(event.definition)
            print("event_payload", event_payload)
            print("event", event)

        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            if "No row was found" in str(e):
                return True
            return False
        return True

    def sendAppointmentReminder(self, channel, msg):
        result = False
        try:
            print("Handling sendAppointmentReminder", msg)

            command_payload = json.loads(msg)
            result = Reminder(command_payload.get("reminder_id"), self.queue_client).sendReminder()

            event = appointmentReminderSent.from_json(appointmentReminderSent, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return result

    def easy2ScheduleAddDataOwnerToEvent(self, channel, msg):
        try:
            print("Handling setEasy2ScheduleExtendedProperties")
            command_payload = json.loads(msg)
            event = easy2scheduleDataOwnerAddedToEvent.from_json(easy2scheduleDataOwnerAddedToEvent, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def easy2scheduleDataOwnerAddedToEvent(self, channel, msg):
        print("Handling easy2scheduleDataOwnerAddedToEvent")
        pass
        try:
            json_data = json.loads(msg)
            new_owners = json_data.get("data")
            activeEvent = Config.db_session.query(Event).filter(Event.owner == json_data.get("owner")).filter(
                Event.event_id == new_owners.get("event_id")).one_or_none()

            if activeEvent is not None:
                current_data_owners = activeEvent.data_owners
                if current_data_owners is None:
                    current_data_owners = "[]"

                current_data_owners = json.loads(current_data_owners)
                merged_owners = current_data_owners + [owner for owner in new_owners.get("owners", []) if
                                                       owner not in current_data_owners]

                activeEvent.data_owners = json.dumps(merged_owners)
                activeEvent.save()
                Config.db_session.commit()

                # exchange = "socketserver"
                # routingKey = "commands"
                # message = {"command": "easy2scheduleDataOwnerAddedToEvent", "args": {}}
                # rooms = get_rooms_by_consumer_id(json_data.get("owner"))
                # for room in rooms:
                #     print("Sending message to room:", room)
                #     payload = {"room": room, "data": message}
                #    channel.basic_publish(exchange=exchange, routing_key=routingKey, body=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False

        return True

    def easy2ScheduleRemoveDataOwnerFromEvent(self, channel, msg):
        try:
            print("Handling easy2ScheduleRemoveDataOwnerFromEvent")
            command_payload = json.loads(msg)
            event = easy2scheduleDataOwnerRemovedFromEvent.from_json(easy2scheduleDataOwnerRemovedFromEvent,
                                                                     command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def easy2scheduleDataOwnerRemovedFromEvent(self, channel, msg):
        print("Handling easy2scheduleDataOwnerAddedToEvent")
        pass
        try:
            json_data = json.loads(msg)
            remove_owners = json_data.get("data")
            activeEvent = Config.db_session.query(Event).filter(Event.owner == json_data.get("owner")).filter(
                Event.event_id == remove_owners.get("event_id")).one_or_none()

            if activeEvent is not None:
                current_data_owners = activeEvent.data_owners
                if current_data_owners is None:
                    current_data_owners = "[]"
                current_data_owners = json.loads(current_data_owners)
                new_data_owners = [owner for owner in current_data_owners if
                                   owner not in remove_owners.get("owners", [])]

                activeEvent.data_owners = json.dumps(new_data_owners)
                activeEvent.save()
                Config.db_session.commit()

                # exchange = "socketserver"
                # routingKey = "commands"
                # message = {"command": "easy2scheduleDataOwnerAddedToEvent", "args": {}}
                # rooms = get_rooms_by_consumer_id(json_data.get("owner"))
                # for room in rooms:
                #     print("Sending message to room:", room)
                #     payload = {"room": room, "data": message}
                #     channel.basic_publish(exchange=exchange, routing_key=routingKey, body=json.dumps(payload))

        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False

        return True

    def setEasy2ScheduleReminderEmailConfiguration(self, channel, msg):
        try:
            print("Handling setEasy2ScheduleReminderEmailConfiguration")
            command_payload = json.loads(msg)
            event = easy2scheduleReminderEmailConfigurationSet.from_json(easy2scheduleReminderEmailConfigurationSet,
                                                                         command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def easy2scheduleReminderEmailConfigurationSet(self, channel, msg):
        try:
            json_data = json.loads(msg)
            account = Config.db_session.query(Account).filter(Account.login == json_data.get("creator")).first()
            Config.db_session.commit()
            if account is not None:
                account_data = json.loads(account.data)
                account_data["reminder_email_configuration"] = json_data.get("data", {}).get("reminder_email_configuration")
                account.data = json.dumps(account_data)
                account.save()

            # exchange = "socketserver"
            # routingKey = "commands"
            # message = {"command": "easy2scheduleReminderEmailConfigurationSaved", "args": json_data.get("correlation_id")}
            # rooms = get_rooms_by_consumer_id(json_data.get("owner"))
            # for room in rooms:
            #     print("Sending message to room:", room)
            #     payload = {"room": room, "data": message}
            #     channel.basic_publish(exchange=exchange, routing_key=routingKey, body=json.dumps(payload))


        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False

        return True

    def setEasy2ScheduleExtendedProperties(self, channel, msg):
        try:
            print("Handling setEasy2ScheduleExtendedProperties")
            command_payload = json.loads(msg)
            event = easy2scheduleExtendedPropertiesSet.from_json(easy2scheduleExtendedPropertiesSet, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def easy2scheduleExtendedPropertiesSet(self, channel, msg):
        try:
            json_data = json.loads(msg)
            Config.db_session.query(ExtendedProperties).filter(
                ExtendedProperties.owner_id == json_data.get("owner")).delete()
            newExtendedProperties = ExtendedProperties()
            newExtendedProperties.creator_id = json_data.get("creator")
            newExtendedProperties.owner_id = json_data.get("owner")
            newExtendedProperties.data = json_data.get("data")

            newExtendedProperties.save()
            Config.db_session.commit()
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False

        return True

    def sendNextWeekReminderEmails(self, channel, body):
        try:
            recipients = CalendarEvents(Config.db_session).sendNextWeekReminderEmails()
            event = nextWeekReminderEmailsSent(recipients)
            event.publish()
        except Exception as e:
            print(e)
            traceback.print_exc()
            sleep(60)
            Config.db_session.rollback()
            return False
        return True

    def updateCalendarColumns(self, channel, msg):
        try:
            print("Handling updateCalendarColumns")
            command_payload = json.loads(msg)
            event = calendarColumnsUpdated.from_json(calendarColumnsUpdated, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def calendarColumnsUpdated(self, channel, msg):
        try:
            json_data = json.loads(msg)
            Config.db_session.query(Columns).filter(Columns.owner_id == json_data.get("owner")).delete()
            newColumns = Columns()
            newColumns.creator_id = json_data.get("creator")
            newColumns.owner_id = json_data.get("owner")
            newColumns.data = json.dumps(json_data.get("data"))
            newColumns.save()
            Config.db_session.commit()
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False

        return True

    def calendarRessourceDeleted(self, channel, msg):
        try:
            json_data = json.loads(msg)
            Config.db_session.query(Ressource).filter(Ressource.ressource_id == json_data.get("ressource_id")).delete()
            Config.db_session.commit()

            message = {"event": "calendarRessourceDeleted",
                       "args": {"correlation_id": json_data.get("correlation_id"),
                                "ressource_id": json_data.get("ressource_id")}}

            payload = {"room": json_data.get("owner"), "data": message}
            channel.basic_publish(exchange="socketserver", routing_key="notifications", body=json.dumps(payload))

        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False

        return True

    def deleteCalendarRessource(self, channel, msg):
        try:
            print("Handling deleteCalendarRessource")
            command_payload = json.loads(msg)
            event = calendarRessourceDeleted.from_json(calendarRessourceDeleted, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def calendarRessourceCreated(self, channel, msg):
        try:
            ressource = Ressource()
            payload = msg

            payload_json = json.loads(payload)

            # Config.db_session.query(Ressource).filter(
            #    Ressource.display_name == payload_json.get("display_name")).filter(
            #    Ressource.owner_id == payload_json.get("owner")).delete()
            # Config.db_session.commit()

            ressource.owner_id = payload_json.get("owner", "not set")
            ressource.creator_id = payload_json.get("creator", "not set")
            ressource.display_name = payload_json.get("display_name", "not set")
            ressource.type = payload_json.get("type", "not set")
            ressource.props = json.dumps(payload_json.get("props", "{}"))
            ressource.ressource_id = payload_json.get("ressource_id", str(uuid.uuid4()))

            ressource.save()
            print("Ressource ID after insert:", ressource.id)

            ressource_event = {"id": ressource.id,
                               "display_name": payload_json.get("display_name"),
                               "type": payload_json.get("type"),
                               "props": payload_json.get("props"),
                               "ressource_id": ressource.ressource_id
                               }

            message = {"event": "calendarRessourceAdded",
                       "args": ressource_event}

            payload = {"room": payload_json.get("owner"), "data": message}
            channel.basic_publish(exchange="socketserver", routing_key="notifications", body=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False
        return True

    def createCalendarRessource(self, channel, msg):
        try:
            print("Handling createCalendarRessource")
            command_payload = json.loads(msg)
            event = calendarRessourceCreated.from_json(calendarRessourceCreated, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def appointmentDeleted(self, channel, msg):
        try:
            print("DELETE EVENT")
            message_payload = json.loads(msg)
            print(message_payload)
            event = Config.db_session.query(Event).filter_by(event_id=message_payload.get("event_id"),
                                                             owner=message_payload.get("owner")).one_or_none()
            if event is None:
                return True

            event.payload = msg
            payload_json = json.loads(event.payload)
            payload_consumer = payload_json.get("creator_consumer_name", "not set")

            Config.db_session.delete(event)
            Config.db_session.commit()
            Config.db_session.flush()

            exchange = "socketserver"
            routingKey = "commands"
            message = {"command": "removeEvent", "args": {"event_id": message_payload.get("event_id")}}
            rooms = get_rooms_by_consumer_id(payload_json.get("owner"))
            for room in rooms:
                payload = {"room": room, "data": message}
                # self.queue_client.publish(toExchange=exchange, routingKey=routingKey, message=json.dumps(payload))
                channel.basic_publish(exchange=exchange, routing_key=routingKey, body=json.dumps(payload))
        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            sleep(5)
            if type(e) == NoResultFound:
                return True

            return False
        return True

    def deleteAppointment(self, channel, msg):
        try:
            print("Handling deleteAppointment")
            command_payload = json.loads(msg)
            event = appointmentDeleted.from_json(appointmentDeleted, command_payload)
            event.publish()

            reminders = Config.db_session.query(EventReminder).filter_by(event_id=command_payload.get("event_id"),
                                                                         owner=command_payload.get("owner")).all()

            for reminder in reminders:
                command_payload["reminder_id"] = reminder.reminder_id
                deleteReminderEvent = easy2scheduleDeleteReminder.from_json(easy2scheduleDeleteReminder, command_payload)
                deleteReminderEvent.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            sleep(5)
            return False

        return True

    def appointmentChanged(self, channel, msg):
        try:
            print("UPDATE EVENT")
            message_payload = json.loads(msg)
            event = Config.db_session.query(Event).filter_by(event_id=message_payload.get("event_id")).one()

            reminders = message_payload.get("reminders", [])
            if message_payload.get("reminders") is not None:
                del message_payload["reminders"]

            msg = json.dumps(message_payload)

            for reminder in reminders:
                reminder_command = setEasy2ScheduleEventReminder(owner=message_payload.get("owner", "not set"),
                                                                 reminder_id=reminder.get(
                                                                     "reminder_id") if reminder.get(
                                                                     "reminder_id") is not None else str(uuid.uuid4()),
                                                                 creator=message_payload.get("creator", "not set"),
                                                                 correlation_id=message_payload.get("correlation_id"),
                                                                 create_time=message_payload.get("create_time"),
                                                                 event_id=event.event_id,
                                                                 reminder_type=reminder.get("type"),
                                                                 seconds_in_advance=reminder.get("seconds_in_advance"),
                                                                 recipients=reminder.get("recipients") if reminder.get(
                                                                     "recipients") is not None else [],
                                                                 )
                reminder_command.publish()

            event.payload = msg
            payload_consumer = message_payload.get("creator_consumer_name", "not set")
            event.owner = message_payload.get("owner", "not set")
            # do not update creator on update
            # event.creator = message_payload.get("creator", "not set")
            event.starttime = message_payload.get("starttime", "not set")
            event.endtime = message_payload.get("endtime", "not set")
            event.data_owners = json.dumps(message_payload.get("data_owners", []))
            event.save()

            if message_payload.get("attachedRessources") is not None:
                if type(message_payload.get("attachedRessources")) == str:
                    attached_ressources = json.loads(message_payload.get("attachedRessources"))
                else:
                    attached_ressources = message_payload.get("attachedRessources")

                if len(attached_ressources) == 1:
                    text_color = json.loads(attached_ressources[0]).get("fontColor", "")
                    color = json.loads(attached_ressources[0]).get("backgroundColor", "")
                elif len(attached_ressources) > 1:
                    text_color = "#000000"
                    color = "#aaaaaa"
                else:
                    text_color = "#000000"
                    color = "#ffffaa"
            else:
                text_color = message_payload.get("textColor")
                color = message_payload.get("color")

            calendar_event = {"id": event.event_id,
                              "resourceId": message_payload.get("resourceId"),
                              "title": message_payload.get("title"),
                              "start": message_payload.get("starttime"),
                              "end": message_payload.get("endtime"),
                              "color": color,
                              "textColor": text_color,
                              "extendedProps": message_payload.get("extended_props", {}),
                              "attachedRessources": message_payload.get("attachedRessources"),
                              "attachedContacts": message_payload.get("attachedContacts"),
                              "data_owners": message_payload.get("data_owners"),
                              "allDay": message_payload.get("allDay", False)
                              }
            exchange = "socketserver"
            routingKey = "commands"

            rooms = get_rooms_by_consumer_id(message_payload.get("owner"))

            for room in rooms:
                message = {"command": "addEvent", "args": filter_eventdata_for_user(login=room, data=calendar_event)}
                payload = {"room": room, "data": message}
                # client().publish(toExchange=exchange, routingKey=routingKey, message=json.dumps(payload))
                channel.basic_publish(exchange=exchange, routing_key=routingKey, body=json.dumps(payload))

                message = {"command": "removeEvent", "args": {"event_id": message_payload.get("event_id")}}
                payload = {"room": room, "data": message}
                # client().publish(toExchange=exchange, routingKey=routingKey, message=json.dumps(payload))
                channel.basic_publish(exchange=exchange, routing_key=routingKey, body=json.dumps(payload))


        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            if "No row was found" in str(e):
                return True
            return False
        return True

    def changeAppointment(self, channel, msg):
        try:
            print("Handling changeAppointment")
            command_payload = json.loads(msg)
            event = appointmentChanged.from_json(appointmentChanged, command_payload)
            event.publish()

        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def createNewAppointment(self, channel, msg):
        try:
            print("Handling createNewAppointment")
            command_payload = json.loads(msg)
            event = newAppointmentCreated.from_json(newAppointmentCreated, command_payload)
            event.publish()
        except Exception as e:
            print(e)
            traceback.print_exc()
            return False

        return True

    def newAppointmentCreated(self, channel, msg):
        try:
            # print("CREATE EVENT")
            event = Event()

            payload_json = json.loads(msg)

            if payload_json.get("event_id") is None or payload_json.get("event_id") == "":
                payload_json["event_id"] = payload_json.get("correlation_id")

            event_exists = Config.db_session.query(Event).filter_by(event_id=payload_json.get("event_id")).one_or_none()
            if event_exists is not None:
                event = event_exists

            reminders = payload_json.get("reminders", [])
            if payload_json.get("reminders") is not None:
                del payload_json["reminders"]

            event.payload = msg
            event.owner = payload_json.get("owner", "not set")
            event.creator = payload_json.get("creator", "not set")
            event.starttime = payload_json.get("starttime", "not set")
            event.endtime = payload_json.get("endtime", "not set")
            event.event_id = payload_json.get("event_id") if len(payload_json.get("event_id")) > 0 else uuid.uuid4()
            event.data_owners = json.dumps(payload_json.get("data_owners", []))

            event.save()

            for reminder in reminders:
                reminder_command = setEasy2ScheduleEventReminder(owner=payload_json.get("owner", "not set"),
                                                                 reminder_id=reminder.get(
                                                                     "reminder_id") if reminder.get(
                                                                     "reminder_id") is not None else str(uuid.uuid4()),
                                                                 creator=payload_json.get("creator", "not set"),
                                                                 correlation_id=payload_json.get("correlation_id"),
                                                                 create_time=payload_json.get("create_time"),
                                                                 event_id=event.event_id,
                                                                 reminder_type=reminder.get("type"),
                                                                 seconds_in_advance=reminder.get("seconds_in_advance"),
                                                                 recipients=reminder.get("recipients") if reminder.get(
                                                                     "recipients") is not None else [],
                                                                 )
                reminder_command.publish()

            # print("Event ID after insert:", event.id)
            if type(event.id) is not int:
                # print("EVENT", event)
                return True
                raise Exception("INSERT IN DATABASE HASN'T RETURNED AN ID")

            if payload_json.get("attachedRessources") is not None:
                if type(payload_json.get("attachedRessources")) == str:
                    attached_ressources = json.loads(payload_json.get("attachedRessources"))
                else:
                    attached_ressources = payload_json.get("attachedRessources")

                if len(attached_ressources) == 1:
                    text_color = json.loads(attached_ressources[0]).get("fontColor", "")
                    color = json.loads(attached_ressources[0]).get("backgroundColor", "")
                elif len(attached_ressources) > 1:
                    text_color = "#000000"
                    color = "#aaaaaa"
                else:
                    text_color = "#000000"
                    color = "#ffffaa"
            else:
                text_color = payload_json.get("textColor")
                color = payload_json.get("color")

            calendar_event = {"id": event.event_id,
                              "resourceId": payload_json.get("resourceId"),
                              "title": payload_json.get("title") if len(payload_json.get("title")) > 0 else " ",
                              "start": payload_json.get("starttime"),
                              "end": payload_json.get("endtime"),
                              "color": color,
                              "textColor": text_color,
                              "extendedProps": payload_json.get("extended_props", {}),
                              "attachedRessources": payload_json.get("attachedRessources"),
                              "attachedContacts": payload_json.get("attachedContacts"),
                              "data_owners": payload_json.get("data_owners"),
                              "allDay": payload_json.get("allDay", False)
                              }

            exchange = "socketserver"
            routingKey = "commands"

            rooms = get_rooms_by_consumer_id(payload_json.get("owner"))
            for room in rooms:
                # print("Sending message to room:", room)

                message = {"command": "addEvent", "args": filter_eventdata_for_user(login=room, data=calendar_event)}
                payload = {"room": room, "data": message}
                channel.basic_publish(exchange=exchange, routing_key=routingKey, body=json.dumps(payload))

        except Exception as e:
            Config.db_session.rollback()
            print(e)
            traceback.print_exc()
            return False

        return True
