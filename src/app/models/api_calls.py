import requests
import os

class ApiCalls():
    def __init__(self):
        pass

    def getGroupMembershipsByApikey(self, apikey):
        url = os.getenv("APIGATEWAY_INTERNAL_BASEURL") + "/get_my_groups"

        headers = {
            'apikey': apikey
        }

        response = requests.request("GET", url, headers=headers)

        return response.json().get("groups", {})

    def getGroupMembershipsByConsumerAndLogin(self, custom_consumer_id, login):
        url = os.getenv("ACCOUNTMANAGER_STORE_BASE_URL") + f"/get_groups_by_login/{custom_consumer_id}/{login}"

        headers = {}

        response = requests.request("GET", url, headers=headers)

        return response.json().get("groups", {})



