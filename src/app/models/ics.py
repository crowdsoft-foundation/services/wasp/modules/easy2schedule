import json
from icalendar import Calendar, Event as CalEvent, vCalAddress, vText
from config import Config
from models.calendar_config import CalendarConfig
from db.event import Event
import os
from datetime import datetime, date
import requests
import pytz
from models.api_calls import ApiCalls


class Ics:
    def __init__(self, owner_id, username):
        self.owner_id = owner_id
        self.username = username

    def generate_ics(self, participant):
        cal = Calendar()
        cal.add('prodid', 'https://www.serviceblick.com')
        cal.add('version', '2.0')

        column_names = self.get_column_names()

        users_group_memberships = ApiCalls().getGroupMembershipsByConsumerAndLogin(self.owner_id, self.username)
        for instance in Config.db_session.query(Event).filter_by(owner=self.owner_id):
            data_owners = json.loads(instance.data_owners)
            matches = [match for match in users_group_memberships if match in data_owners]
            if f"user.{self.username}" in data_owners:
                matches.append(f"user.{self.username}")

            if len(matches) == 0 and len(data_owners) > 0:
                continue


            skip = False
            event_data = json.loads(instance.payload)
            event = CalEvent()

            ressource_name = column_names.get(event_data.get("resourceId"), event_data.get("resourceId"))

            if event_data.get("allDay") == True:
                start_date_object, end_date_object = self.get_all_day_event_dates(event_data)
            else:
                start_date_object, end_date_object = self.get_event_dates(event_data)

            if participant is not None and len(event_data.get("attachedRessources")) > 0:
                for entry in event_data.get("attachedRessources"):
                    if participant not in entry:
                        skip = True

            if skip is True:
                continue

            event.add('summary', event_data.get("title") + " - " + ressource_name)
            event.add('location', ressource_name)
            event.add('dtstart', start_date_object)
            end_date_object and event.add('dtend', end_date_object)

            date_format = '%Y-%m-%d %H:%M:%S'
            if event_data.get("create_time") is None:
                event.add('dtstamp', datetime.now())
            else:
                event.add('dtstamp', datetime.strptime(event_data.get("create_time"), date_format))
            event['uid'] = event_data.get("correlation_id")
            event.add('priority', 5)

            for ressource in event_data.get("attachedRessources"):
                ressource_json = json.loads(ressource)
                self.addRequestedAttendee(event, attendee_name=ressource_json.get("name"), mail="")

            cal.add_component(event)

        ics = cal.to_ical()
        return ics

    def addOrganizer(self, event, organizer_name, mail=None):
        #organizer = vCalAddress(f'MAILTO:{mail if mail else "nomail"}')
        organizer = vCalAddress(f'MAILTO:{mail}' if mail else "invalid:nomail")
        organizer.params['cn'] = vText(organizer_name)
        organizer.params['role'] = vText('CHAIR')
        event['organizer'] = organizer

    def addRequestedAttendee(self, event, attendee_name, mail=None):
        #attendee = vCalAddress(f'MAILTO:{mail if mail else "nomail"}')
        attendee = vCalAddress(f'MAILTO:{mail}' if mail else "invalid:nomail")
        attendee.params['cn'] = vText(attendee_name)
        attendee.params['ROLE'] = vText('REQ-PARTICIPANT')
        event.add('attendee', attendee, encode=0)

    def addOptionalAttendee(self, event, attendee_name, mail=None):
        #attendee = vCalAddress(f'MAILTO:{mail if mail else "nomail"}')
        attendee = vCalAddress(f'MAILTO:{mail}' if mail else "invalid:nomail")
        attendee.params['cn'] = vText(attendee_name)
        attendee.params['ROLE'] = vText('OPT-PARTICIPANT')
        event.add('attendee', attendee, encode=0)

    def addNonPersonAttendee(self, event, attendee_name, mail=None):
        #attendee = vCalAddress(f'MAILTO:{mail if mail else "nomail"}')
        attendee = vCalAddress(f'MAILTO:{mail}' if mail else "invalid:nomail")
        attendee.params['cn'] = vText(attendee_name)
        attendee.params['ROLE'] = vText('NON-PARTICIPANT')
        event.add('attendee', attendee, encode=0)

    def addChairmanAttendee(self, event, attendee_name, mail=None):
        #attendee = vCalAddress(f'MAILTO:{mail if mail else "nomail"}')
        attendee = vCalAddress(f'MAILTO:{mail}' if mail else "invalid:nomail")
        attendee.params['cn'] = vText(attendee_name)
        attendee.params['ROLE'] = vText('CHAIR')
        event.add('attendee', attendee, encode=0)

    def addRessource(self, event, ressource_name):
        ressources = vText(ressource_name)
        event.add('resources', ressources, encode=0)

    def get_column_names(self):
        column_json = CalendarConfig(owner_id=self.owner_id).get_columns()
        column_names = {}
        for column_json_key in column_json.keys():
            for property_list in column_json[column_json_key]:
                column_names[property_list.get("id")] = property_list.get("title")

        return column_names

    def get_event_dates(self, event_data):
        date_format = '%Y-%m-%d %H:%M:%S'
        start_date_object = datetime.strptime(event_data.get("starttime"), date_format)
        end_date_object = datetime.strptime(event_data.get("endtime"), date_format)
        local_tz = pytz.timezone('Europe/Berlin')
        return (local_tz.localize(start_date_object), local_tz.localize(end_date_object))

    def get_all_day_event_dates(self, event_data):
        date_format = '%Y-%m-%d'
        starttime = event_data.get("starttime").replace("T", " ").split(" ")
        endtime = event_data.get("endtime").replace("T", " ").split(" ")

        dt = datetime.strptime(starttime[0], date_format)
        start_date_object = date(year=dt.year, month=dt.month, day=dt.day)
        dt = datetime.strptime(endtime[0], date_format)
        end_date_object = date(year=dt.year, month=dt.month, day=dt.day)
        if start_date_object == end_date_object:
            end_date_object = None

        return (start_date_object, end_date_object)
