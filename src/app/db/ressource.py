from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence, UniqueConstraint
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()

class Ressource(DbEntity, Base):
    __tablename__ = 'ressource'
    __table_args__ = (UniqueConstraint('ressource_id', name='ressource_id'), {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'mysql_collate': 'utf8_general_ci'})

    id = Column(Integer, primary_key=True, autoincrement=True)
    display_name = Column(String(255))
    creator_id = Column(String(255))
    owner_id = Column(String(255))
    type = Column(String(255))
    props = Column(Text)
    ressource_id = Column(String(255))

    def __init__(self):
        super().__init__()
        Base.metadata.create_all(Config.db_engine)

    def __repr__(self):
        return "<Ressource(id='%s', display_name='%s', ressource_id='%s')>\r\n" % (self.id, self.display_name, self.ressource_id)

    def save(self):
        try:
            self.session.add(self)
            self.session.commit()
            self.session.flush()
        except Exception as e:
            print(e)
            self.rollback()

    def commit(self):
        self.session.commit()

    def add(self):
        self.session.add(self)

    def rollback(self):
        self.session.rollback()


