from db.abstract import Migrate
from sqlalchemy import types

def migrate_db():
    migration = Migrate()

    # 28.10.2021
    #migration.execute("Rename login_id json field to account_id", "UPDATE ressource set props = REPLACE(props, '\"login_id\":', '\"account_id\":')")
    migration.add_column(table_name="accounts", column_name="login_id", data_type=types.VARCHAR(255))
    migration.execute("Add unique contraint to login_id-column", "ALTER TABLE `accounts` ADD UNIQUE INDEX `login_id` (`login_id`);")
    migration.add_column(table_name="events", column_name="event_id", data_type=types.VARCHAR(255))
    migration.execute("Add unique contraint to event_id-column", "ALTER TABLE `events` ADD UNIQUE INDEX `event_id` (`event_id`);")
    migration.execute("Calculate id for event_id if null", "UPDATE `events` set event_id = CONCAT('lgcy_',UUID()) where event_id is null;")
    migration.add_column(table_name="ressource", column_name="ressource_id", data_type=types.VARCHAR(255))
    migration.execute("Add unique contraint to ressource_id-column",
                      "ALTER TABLE `ressource` ADD UNIQUE INDEX `ressource_id` (`ressource_id`);")
    migration.execute("Calculate id for ressource_id if null",
                      "UPDATE `ressource` set ressource_id = CONCAT('lgcy_',UUID()) where ressource_id is null;")
    migration.add_column(table_name="events", column_name="data_owners", data_type=types.TEXT())
    default_data_owners = '[]'
    migration.execute("Set default data_owners",
                      f"UPDATE `events` set data_owners = '{default_data_owners}' where data_owners is null;")


