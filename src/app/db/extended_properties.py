from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()

class ExtendedProperties(DbEntity, Base):
    __tablename__ = 'extendedproperties'
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'mysql_collate': 'utf8_general_ci'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    creator_id = Column(String(255))
    owner_id = Column(String(255))
    data = Column(Text)

    def __init__(self):
        super().__init__()
        Base.metadata.create_all(Config.db_engine)

    def __repr__(self):
        return "<Ressource(id='%s', owner_id='%s')>\r\n%s" % (self.id, self.owner_id, self.data)

    def save(self):
        try:
            self.session.add(self)
            self.session.commit()
            self.session.flush()
        except Exception as e:
            print(e)
            self.rollback()

    def commit(self):
        self.session.commit()

    def add(self):
        self.session.add(self)

    def rollback(self):
        self.session.rollback()


