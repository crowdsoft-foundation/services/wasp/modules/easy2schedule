from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence, UniqueConstraint
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base
from pblib.amqp import client

Base = declarative_base()

class Event(DbEntity, Base):
    __tablename__ = 'events'
    __table_args__ = (UniqueConstraint('event_id', name='event_id'),
                      {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_general_ci'})

    id = Column(Integer, primary_key=True, autoincrement=True)
    owner = Column(String(255))
    creator = Column(String(255))
    payload = Column(Text)
    starttime = Column(DateTime)
    endtime = Column(DateTime)
    event_id = Column(String(255))
    data_owners = Column(Text, default='[]')

    def __init__(self):
        super().__init__()
        Base.metadata.create_all(Config.db_engine)

    def __repr__(self):
        return "<Event(id='%s', event_id='%s' owner='%s', creator='%s', data_owners='%s')>\r\n%s" % (self.id, self.event_id, self.owner, self.creator, self.data_owners, self.payload)

    def save(self):
        try:
            self.session.add(self)
            self.session.commit()
            self.session.flush()
        except Exception as e:
            self.rollback()

    def commit(self):
        self.session.commit()

    def add(self):
        self.session.add(self)

    def rollback(self):
        self.session.rollback()


