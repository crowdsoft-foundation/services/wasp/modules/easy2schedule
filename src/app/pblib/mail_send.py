import os
import os.path
import datetime
from M2Crypto import BIO, Rand, SMIME
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
import boto3
import smtplib, ssl

from email.message import Message
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

from jinja2 import Environment, FileSystemLoader
import requests

# TODO MOUNT CERTS INTO CONTAINER INSTEAD OF NEEDING THEM IN THE REPOSITORY
signMail = True
if os.path.isfile(os.getenv("MAIL_CERTIFICATES_KEY_PATH")):
    ssl_key = os.getenv("MAIL_CERTIFICATES_KEY_PATH")
else:
    signMail = False
if os.path.isfile(os.getenv("MAIL_CERTIFICATES_CRT_PATH")):
    ssl_cert = os.getenv("MAIL_CERTIFICATES_CRT_PATH")
else:
    signMail = False


sender = 'no-reply@serviceblick.com'
nextweekreminder_mail = '/src/app/ressources/nextweekreminder_mail.txt'

file_loader = FileSystemLoader('/src/app/ressources')
env = Environment(loader=file_loader)

def get_account_signature(account_id):
    url = os.getenv("ACCOUNTMANAGER_STORE_BASE_URL") + f"/get_account_profile?account_id={account_id}"

    response = requests.request("GET", url)

    signature = response.json().get("email_signature")
    return signature

def send_reminder_mail(recipient, event_table, events, email_config):
    subject = 'Deine Termine für nächste Woche'
    template_text = env.get_template('nextweekreminder_mail.txt')
    template_html = env.get_template('nextweekreminder_mail.html')



    try:
        to = [recipient]
        login = os.getenv("SMTP_USERNAME")
        password = os.getenv("SMTP_PASSWORD")

        with open(nextweekreminder_mail, "r") as cmf:
            body = cmf.read()

        signature = get_account_signature(email_config["consumer_id"])
        body = template_text.render(event_table=event_table, events=events, email_config=email_config, signature=signature)
        body_html = template_html.render(event_table=event_table, events=events, email_config=email_config, signature=signature)
        msg = MIMEMultipart('alternative')

        basemsg = MIMEText(body, 'plain')
        msg.attach(basemsg)

        basemsg_html = MIMEText(body_html, 'html')
        msg.attach(basemsg_html)



        if signMail:
            msg_str = msg.as_string()
            buf = BIO.MemoryBuffer(msg_str.encode())

            # load seed file for PRNG
            Rand.load_file('/tmp/randpool.dat', -1)
            smime = SMIME.SMIME()

            # load certificate
            smime.load_key(ssl_key, ssl_cert)

            # sign whole message
            p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

            # create buffer for final mail and write header
            out = BIO.MemoryBuffer()
            out.write('From: %s\n' % sender)
            out.write('To: %s\n' % COMMASPACE.join(to))
            out.write('Date: %s\n' % formatdate(localtime=True))
            out.write('Subject: %s\n' % subject)
            out.write('Auto-Submitted: %s\n' % 'auto-generated')

            # convert message back into string
            buf = BIO.MemoryBuffer(msg_str.encode())

            # append signed message and original message to mail header
            smime.write(out, p7, buf)

            # load save seed file for PRNG
            Rand.save_file('/tmp/randpool.dat')

            content = out.read()
        else:
            msg['Subject'] = subject
            msg['From'] = sender
            msg['To'] = COMMASPACE.join(to)
            content = msg.as_string()


        # Log in to server using secure context and send email
        if os.getenv("SMTP_SSL") == "true":
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()
        else:
            with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()


        print('NextWeekReminder mail sent')
        return True
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())
        return False


def send_single_event_reminder_mail(recipient, event_payload):
    subject = 'Erinnerung an einen Easy2Schedule Termin'
    template_text = env.get_template('singleventreminder_mail.txt')
    template_html = env.get_template('singleventreminder_mail.html')

    try:
        to = [recipient]
        login = os.getenv("SMTP_USERNAME")
        password = os.getenv("SMTP_PASSWORD")

        signature = get_account_signature(event_payload.get("owner"))

        starttime = datetime.datetime.fromisoformat(event_payload.get("starttime")).strftime("%d.%m.%Y, %H:%M")
        endtime = datetime.datetime.fromisoformat(event_payload.get("endtime")).strftime("%d.%m.%Y, %H:%M")
        event_time = starttime + " - " + endtime

        body = template_text.render(event_time=event_time, event_title=event_payload.get("title"), event=event_payload, signature=signature)
        body_html = template_html.render(event_time=event_time, event_title=event_payload.get("title"), event=event_payload, signature=signature)
        msg = MIMEMultipart('alternative')

        basemsg = MIMEText(body, 'plain')
        msg.attach(basemsg)

        basemsg_html = MIMEText(body_html, 'html')
        msg.attach(basemsg_html)



        if signMail:
            msg_str = msg.as_string()
            buf = BIO.MemoryBuffer(msg_str.encode())

            # load seed file for PRNG
            Rand.load_file('/tmp/randpool.dat', -1)
            smime = SMIME.SMIME()

            # load certificate
            smime.load_key(ssl_key, ssl_cert)

            # sign whole message
            p7 = smime.sign(buf, SMIME.PKCS7_DETACHED)

            # create buffer for final mail and write header
            out = BIO.MemoryBuffer()
            out.write('From: %s\n' % sender)
            out.write('To: %s\n' % COMMASPACE.join(to))
            out.write('Date: %s\n' % formatdate(localtime=True))
            out.write('Subject: %s\n' % subject)
            out.write('Auto-Submitted: %s\n' % 'auto-generated')

            # convert message back into string
            buf = BIO.MemoryBuffer(msg_str.encode())

            # append signed message and original message to mail header
            smime.write(out, p7, buf)

            # load save seed file for PRNG
            Rand.save_file('/tmp/randpool.dat')

            content = out.read()
        else:
            msg['Subject'] = subject
            msg['From'] = sender
            msg['To'] = COMMASPACE.join(to)
            content = msg.as_string()


        # Log in to server using secure context and send email
        if os.getenv("SMTP_SSL") == "true":
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT"), context=context) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()
        else:
            with smtplib.SMTP(os.getenv("SMTP_HOST"), os.getenv("SMTP_PORT")) as server:
                server.ehlo()
                if os.getenv("SMTP_AUTH_NEEDED") == "true":
                    server.login(login, password)
                server.sendmail(sender, to, content)
                server.close()


        print('send_single_event_reminder_mail mail sent')
        return True
    except Exception as e:
        print(e)
        import traceback
        print(traceback.format_exc())
        return False


