#!/usr/bin/env python
import socketio

sio = socketio.Client(logger=False, engineio_logger=False)
sio.connect("http://socket-server.planblick.svc:5000")


@sio.event
def connect():
    print("I'm connected!")


@sio.event
def connect_error():
    print("The connection failed!")


@sio.event
def disconnect():
    print("I'm disconnected!")


@sio.on('command')
def on_message(data):
    if (data.get("command") == "fireEvent"):
        args = data.get("args", {})
        eventdata = args.get("eventdata", {})
        if (args.get("eventname") == "addContact"):
            payload = {
                "command": "fireEvent",
                "args": {
                    "eventname": "contactAdded",
                    "eventdata": eventdata
                },
                "room": args.get("consumer_id")
            }

            sio.emit('command', payload)
            payload["room"] = "tmp_worker"
            sio.emit('command', payload)

        elif (args.get("eventname") == "removeContact"):
            payload = {
                "command": "fireEvent",
                "args": {
                    "eventname": "contactAdded",
                    "eventdata": eventdata
                },
                "room": args.get("consumer_id")
            }

            sio.emit('command', payload)


sio.emit('join', {"room": "tmp_worker"});
