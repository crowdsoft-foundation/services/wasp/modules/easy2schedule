from flask import Flask, jsonify, request, redirect, render_template, url_for, render_template, Response
from time import strftime, sleep
import json
import os
from planblick.logger import Logger
from config import Config
from db.event import Event
from db.ressource import Ressource
import subprocess
from datetime import datetime
from db.account import Account
from db.columns import Columns
from db.extended_properties import ExtendedProperties
from pblib.helpers import filter_dict
import csv

from models.calendar_events import CalendarEvents
from models.api_calls import ApiCalls
from models.ics import Ics
from models.calendar_config import CalendarConfig
from db.event_reminder import EventReminder


app = Flask(__name__)
#TODO: use sio instead of this custom log-server implementation
#logserver_host = json.loads(os.getenv("CLUSTER_CONFIG")).get("logserver")
#log = Logger(logserver_host, 9999, consoleLog=True if len(logserver_host) > 0 else True).getLogger()


@app.route('/personal_events', methods=["GET"])
def personal_events_action():
    apikey = request.headers.get("apikey") if request.headers.get("apikey") is not None else request.args.get("apikey")
    start_date = datetime.fromisoformat(request.args.get("start"))
    end_date = datetime.fromisoformat(request.args.get("end"))
    events =  CalendarEvents(db_session=Config.db_session).get_personal_events_by_apikey(apikey=apikey, start_date=start_date, end_date=end_date)
    #events = CalendarEvents(db_session=Config.db_session).get_personal_events_by_login_id(login_id="eeee1eeb-86f3-4e9c-9192-21a305862f24",
    #                                                                                    start_date=start_date,
    #                                                                                    end_date=end_date)

    if request.args.get("as_ics", "0") == "0":
        return jsonify(events), 200
    else:
        cal = Calendar()
        cal.add('prodid', 'https://www.serviceblick.com')
        cal.add('version', '2.0')
        event = CalEvent()

        view_data = Config.db_session.query(Columns).filter_by(
            owner_id=request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")).all()
        Config.db_session.commit()
        column_names = {}
        for column_data in view_data:
            column_json = json.loads(column_data.data)
            for column_json_key in column_json.keys():
                for property_list in column_json[column_json_key]:
                    column_names[property_list.get("id")] = property_list.get("title")

        for instance in events:
            print(instance)
            event_data = instance
            event = CalEvent()
            print("COLUMN NAMES", event_data)
            ressource_name = column_names.get(event_data.get("resourceId"), event_data.get("resourceId"))
            event.add('summary', event_data.get("title") + " - " + ressource_name)
            event.add('location', ressource_name)

            timestamp = datetime.now()
            end_date_object = None
            if event_data.get("allDay") == True:
                date_format = '%Y-%m-%d'
                starttime = event_data.get("starttime").split(" ")
                endtime = event_data.get("endtime").split(" ")

                from datetime import date
                dt = datetime.strptime(starttime[0], date_format)
                start_date_object = date(year=dt.year, month=dt.month, day=dt.day)
                dt = datetime.strptime(endtime[0], date_format)
                end_date_object = date(year=dt.year, month=dt.month, day=dt.day)
                if start_date_object == end_date_object:
                    end_date_object = None
            else:
                date_format = '%Y-%m-%d %H:%M:%S'
                start_date_object = datetime.strptime(event_data.get("starttime"), date_format)
                end_date_object = datetime.strptime(event_data.get("endtime"), date_format)

            event.add('dtstart', start_date_object)
            if end_date_object is not None:
                event.add('dtend', end_date_object)
            event.add('dtstamp', timestamp)
            event['uid'] = event_data.get("correlation_id")
            event.add('priority', 5)

            cal.add_component(event)

        ics = cal.to_ical()
        # return jsonify(event_data)
        return ics, 200, {'Content-Type': 'text/calendar; charset=utf-8'}


@app.route('/event/<event_ids>', methods=["GET"])
def event_by_id_action(event_ids):
    account_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    event_ids = event_ids.split(",")
    result = None
    if(len(event_ids) == 1):
        result = Config.db_session.query(Event).filter(Event.owner == account_id).filter(Event.event_id == event_ids).one_or_none()
        data = json.loads(result.payload)
        data["event_id"] = result.event_id
        return jsonify(data)
    else:
        result = Config.db_session.query(Event).filter(Event.owner == account_id).filter(Event.event_id.in_(event_ids)).all()
        return_values = []
        for entry in result:
            data = json.loads(entry.payload)
            data["event_id"] = entry.event_id
            return_values.append(data)
        return jsonify(return_values)
    if result is None:
        return "", 404

    else:
        return {}, 404

@app.route('/my_event_reminders/<event_id>', methods=["GET"])
def my_event_reminders_action(event_id):
    account_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey") if request.headers.get("apikey") is not None else request.args.get("apikey")
    login = Account().getLoginByApikey(apikey)
    result = Config.db_session.query(EventReminder).filter(EventReminder.owner == account_id).filter(EventReminder.event_id == event_id).all()
    returnvalue = []
    for row in result:
        if row.data_owners and login in json.loads(row.data_owners):
            definition = json.loads(row.definition)
            returnvalue.append({"reminder_id": row.reminder_id, "event_id": row.event_id, "type": definition.get("reminder_type"), "seconds_in_advance": row.seconds_in_advance, "recipients": definition.get("recipients")})

    return jsonify(returnvalue)

@app.route('/events', methods=["GET"])
def events_action():
    print("FETCHING EVENTS")
    account_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    apikey = request.headers.get("apikey") if request.headers.get("apikey") is not None else request.args.get("apikey")
    login = Account().getLoginByApikey(apikey)
    as_csv = True if request.args.get("format") == "csv" else False

    print(request.headers.get("X-Credential-Username", "UNKNOWN"))

    # data["username"] = request.headers.get("X-Credential-Username", "UNKNOWN")
    # data["consumername"] = request.headers.get("X-Consumer-Username", "UNKNOWN")
    events = []
    start_date = datetime.fromisoformat(request.args.get("start"))
    end_date = datetime.fromisoformat(request.args.get("end"))

    result = json.loads(Account().getData(apikey=apikey))
    users_group_memberships = ApiCalls().getGroupMembershipsByApikey(apikey)
    print("RESULT GROUPS", users_group_memberships)

    if result.get("event_data_filter"):
        event_data_filter = result.get("event_data_filter")
    else:
        event_data_filter = None

    sql = f'SELECT * FROM events WHERE owner=:account_id AND  (endtime > :start_date AND starttime BETWEEN :start_date AND :end_date OR starttime < :end_date AND endtime BETWEEN :start_date AND "{end_date}"  OR (starttime < :start_date AND endtime > :end_date))'
    results = Config.db_session.execute(sql, {"start_date": start_date, "end_date": end_date, "account_id": account_id}).fetchall()
    for instance in results:
        data_owners = json.loads(instance.data_owners)
        matches = [match for match in users_group_memberships if match in data_owners]
        if f"user.{login}" in data_owners:
            matches.append(f"user.{login}")
        
        anonymize = False
        in_background = False
        if len(matches) == 0 and len(data_owners) > 0:
            anonymize = True
            in_background = True


        event_data = instance.payload
        event_data = json.loads(event_data)


        if event_data.get("attachedRessources") is not None:
            if type(event_data.get("attachedRessources")) == str:
                attached_ressources = json.loads(event_data.get("attachedRessources"))
            else:
                attached_ressources = event_data.get("attachedRessources")

            if len(attached_ressources) == 1:
                text_color = json.loads(attached_ressources[0]).get("fontColor", "")
                color = json.loads(attached_ressources[0]).get("backgroundColor", "")
            elif len(attached_ressources) > 1:
                text_color = "#000000"
                color = "#aaaaaa"
            else:
                text_color = "#000000"
                color = "#ffffaa"
        else:
            text_color = event_data.get("textColor")
            color = event_data.get("color")

        calendar_event = {"id": instance.event_id if instance.event_id is not None else event_data.get("correlation_id"),
                  "resourceId": event_data.get("resourceId"),
                  "title": event_data.get("title") if not anonymize and len(event_data.get("title")) > 0 else " " if not anonymize else "Anonymized",
                  "start": event_data.get("starttime"),
                  "end": event_data.get("endtime"),
                  "color": color,
                  "textColor": text_color,
                  "classNames": ["defaultEvent"],
                  "extendedProps": event_data.get("extended_props", {}) if not anonymize else {},
                  "attachedRessources": event_data.get("attachedRessources") if not anonymize else [],
                  "attachedContacts": event_data.get("attachedContacts") if not anonymize else [],
                  "data_owners": event_data.get("data_owners") if not anonymize else ["none"],
                  #"reminders": event_data.get("reminders"),
                  "allDay": event_data.get("allDay", False),
                  "rendering": "background" if in_background else "",
                  "resourceEditable": not anonymize
                }

        # "rendering": "background" if event_data.get("allDay", False) else ""}

        # filter event if special filter is set for user
        # this is just a dummy for now
        #print("DATAFILTER APPLIED:", event_data_filter)
        if event_data_filter is not None and event_data.get("extended_props", {}).get("creator_login") != login:
            filter = event_data_filter
            # calendar_event = {**calendar_event, **filter}
            calendar_event = filter_dict(filter, calendar_event)

        events.append(calendar_event)

    if not as_csv:
        return jsonify(events), 200
    else:
        view_data = Config.db_session.query(Columns).filter_by(owner_id=account_id).all()
        Config.db_session.commit()
        column_names = {}
        for column_data in view_data:
            column_json = json.loads(column_data.data)
            for column_json_key in column_json.keys():
                for property_list in column_json[column_json_key]:
                    column_names[property_list.get("id")] = property_list.get("title")

        dataset = events
        csv_filename = "/tmp/" + apikey + ".csv"
        with open(csv_filename, mode='w', newline='', encoding='utf-8-sig') as file:
            csv_writer = csv.writer(file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            csv_headers = ["start", "end", "title","ressource", "attachedRessources"]
            rows = []
            for app in dataset:
                columns = []
                columns.append(app["start"])
                columns.append(app["end"])
                columns.append(app["title"])
                columns.append(column_names.get(app["resourceId"], app["resourceId"]))


                json_ressources = app["attachedRessources"]
                ressources = []
                for ressource in json_ressources:
                    ressource = json.loads(ressource)
                    ressources.append(ressource.get("name"))

                columns.append(",".join(ressources))

                for key in sorted(app["extendedProps"]):
                    value = app["extendedProps"][key]
                    # key, value = prop
                    if key.startswith("ep_"):
                        key = str(key.replace("ep_", ""))
                        columns.append(value)
                        if key not in csv_headers:
                            csv_headers.append(key)

                rows.append(columns)

            csv_writer.writerow(csv_headers)

            for row in rows:
                csv_writer.writerow(row)

        with open(csv_filename, "r") as fp:
            csv_file = fp.read()
            print("CSVFILE", csv_file, csv_filename)
            #os.remove(csv_filename)
        return Response(
            csv_file,
            mimetype="text/csv", headers={"Content-disposition": "attachment; filename=export.csv"})


@app.route('/ressources', methods=["GET"])
def ressources_action():
    account_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    # data["username"] = request.headers.get("X-Credential-Username", "UNKNOWN")
    # data["consumername"] = request.headers.get("X-Consumer-Username", "UNKNOWN")
    ressources = []
    print(account_id)
    for instance in Config.db_session.query(Ressource).filter_by(owner_id=account_id):
        ressource_props = instance.props
        ressource_props = json.loads(ressource_props)
        calendar_ressource = {"id": instance.id,
                              "display_name": instance.display_name,
                              "type": instance.type,
                              "props": json.loads(instance.props),
                              "ressource_id": instance.ressource_id
                              }

        ressources.append(calendar_ressource)
    return jsonify(ressources), 200


@app.route('/columns', methods=["GET"])
def columns_action():
    account_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    data = CalendarConfig(owner_id=account_id).get_columns()
    return jsonify(data), 200


@app.route('/extended_properties', methods=["GET"])
def extended_properties_action():
    account_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    data = Config.db_session.query(ExtendedProperties).filter_by(owner_id=account_id).first()
    Config.db_session.commit()
    if data is not None:
        return data.data, 200
    else:
        return "", 200


@app.route('/account_headers', methods=["GET"])
def account_headers_action():
    apikey = request.headers.get("apikey") if request.headers.get("apikey") is not None else request.args.get("apikey")

    result = json.loads(Account().getData(apikey=apikey))
    print("HEADERS", result)
    if result.get("headers"):
        print("USING CUSTOM HEADERS")
        calendar_headers = result.get("headers")
    else:
        calendar_headers = "prev,previousday,today,nextday,next,timeGridDay,resourceTimeGridMultiDay,resourceTimeGridMultiDay3,resourceTimeGridDay,resourceTimeGridDay2,dayGridMonth,resourceTimelineFourDays,filter"
    print("HEADERS", calendar_headers)
    return jsonify({"headers_left": calendar_headers}), 200


@app.route('/account_config', methods=["GET"])
def account_config_action():
    apikey = request.headers.get("apikey") if request.headers.get("apikey") is not None else request.args.get("apikey")

    result = json.loads(Account().getData(apikey=apikey))

    print("CONFIG", result)
    return jsonify({"user_config": result}), 200

@app.route('/email_config', methods=["GET"])
def email_config_action():
    apikey = request.headers.get("apikey") if request.headers.get("apikey") is not None else request.args.get("apikey")
    loginData = Account().getLoginDataByApikey(apikey)

    email_config = CalendarEvents(Config.db_session).getReminderEmailConfigs(loginData.get("login"))
    if len(email_config) > 0:
        return jsonify(email_config.pop()), 200
    else:
        return jsonify([]), 200


@app.route('/ics', methods=["GET"])
@app.route('/ics/<participant>')
def ics_action(participant=None):
    owner_id = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
    username = request.headers.get("X-Consumer-Username", "UNKNOWN")
    ics = Ics(owner_id=owner_id, username=username).generate_ics(participant=participant)
    
    return ics, 200, {'Content-Type': 'text/calendar; charset=utf-8'}


@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@app.route('/metrics', methods=["GET"])
def metrics_action():
    command = 'top -b -n 1'  # or whatever you use
    output = subprocess.Popen(command, stdout=subprocess.PIPE).communicate[0]

    # parse output here and extract cpu usage. this is super-dependent
    # on the layout of your system monitor output
    cpuline = output.split('\n')[2]

    # +like you did here:
    print(output)
    return jsonify("NYI"), 200


@app.route('/network', methods=["GET"])
def network_action():
    network_config = Config()
    network_config = network_config.bindings

    return jsonify(network_config), 200


@app.route('/favicon.ico', methods=["GET"])
def favicon_action():
    return redirect("/static/favicon.ico", code=302)


@app.before_request
def before_request():
    payload = request.get_json(silent=True)
    if payload is not None:
        payload["correlation_id"] = request.headers.get("correlation_id", "")
        request.data = json.dumps(payload)


@app.teardown_appcontext
def shutdown_session(exception=None):
    Config.db_session.remove()


@app.after_request
def after_request(response):
    """ Logging after every request. """
    if request.full_path not in ["/metrics?", "/metrics", "/healthz", "/healthz?"]:
        data = {}
        data["account_id"] = request.headers.get("X-Consumer-Custom-Id", "UNKNOWN")
        data["username"] = request.headers.get("X-Credential-Username", "UNKNOWN")
        data["consumername"] = request.headers.get("X-Consumer-Username", "UNKNOWN")
        # log(" |-| ".join([json.dumps(data), request.remote_addr, request.method, request.scheme, request.full_path,
        #                   response.status]))
    return response
